package com.dbshac2hire.thankcoin.ledger.api.controller;

import java.util.Date;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbshac2hire.thankcoin.ledger.api.controller.util.ErrorResponseEntity;
import com.dbshac2hire.thankcoin.ledger.api.controller.util.TotalBalanceHolder;
import com.dbshac2hire.thankcoin.ledger.api.domain.Account;
import com.dbshac2hire.thankcoin.ledger.api.repository.AccountRepository;

@Controller
@EnableAutoConfiguration
public class AccountController {
	private static final int MAX_CUSTOM_ACCOUNT_NUMBER_QRY = 100;

	private static final String DEFAULT_CUSTOM_ACCOUNT_NUMBER_IN_QRY = "5";

	private static final Logger log = Logger.getLogger(AccountController.class);

	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(value = "/account", method = { RequestMethod.POST })
	@ResponseBody
	ResponseEntity<?> createAccount(@RequestBody CreateAccountRequest createAccountRequest) {
		if (createAccountRequest.getNumber() == null || createAccountRequest.getNumber().trim().isEmpty()) {
			log.error("Error while creating account: empty number");
			return new ErrorResponseEntity("Emptry account number is prohibited");
		}
		Date currentTime = new Date();
		try {
			Account account = accountRepository.save(new Account(createAccountRequest.getNumber(),
					createAccountRequest.isOpen(), 0, currentTime, currentTime));
			return new ResponseEntity<>(account, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error while creating account", e);
			return new ErrorResponseEntity("Unexpected error while creating account");
		}
	}

	@RequestMapping("/account/{number}")
	@ResponseBody
	ResponseEntity<Account> getAccount(@PathVariable String number) {
		Optional<Account> account = accountRepository.findByNumber(number);
		if (account.isPresent()) {
			return new ResponseEntity<Account>(account.get(), HttpStatus.OK);
		}
		return new ResponseEntity<Account>((Account) null, HttpStatus.NOT_FOUND);
	}

	@RequestMapping("/accounts")
	@ResponseBody
	Iterable<Account> getAccounts() {
		return accountRepository.findAll();
	}

	@RequestMapping("/topaccounts")
	@ResponseBody
	ResponseEntity<?> getTopAccounts(
			@RequestParam(required = false, defaultValue = DEFAULT_CUSTOM_ACCOUNT_NUMBER_IN_QRY) int number) {
		if (number < 1 || number > MAX_CUSTOM_ACCOUNT_NUMBER_QRY) {
			return new ErrorResponseEntity("Number out of range 1.." + MAX_CUSTOM_ACCOUNT_NUMBER_QRY);
		}
		return new ResponseEntity<>(accountRepository.findAllByOrderByBalanceDesc(new PageRequest(0, number)),
				HttpStatus.OK);
	}

	@RequestMapping("/recentaccounts")
	@ResponseBody
	ResponseEntity<?> getRecentAccounts(
			@RequestParam(required = false, defaultValue = DEFAULT_CUSTOM_ACCOUNT_NUMBER_IN_QRY) int number) {
		if (number < 1 || number > MAX_CUSTOM_ACCOUNT_NUMBER_QRY) {
			return new ErrorResponseEntity("Number out of range 1.." + MAX_CUSTOM_ACCOUNT_NUMBER_QRY);
		}
		return new ResponseEntity<>(accountRepository.findAllByOrderByCreatedDesc(new PageRequest(0, number)),
				HttpStatus.OK);
	}

	@RequestMapping("/recentactiveaccounts")
	@ResponseBody
	ResponseEntity<?> getRecentActiveAccounts(
			@RequestParam(required = false, defaultValue = DEFAULT_CUSTOM_ACCOUNT_NUMBER_IN_QRY) int number) {
		if (number < 1 || number > MAX_CUSTOM_ACCOUNT_NUMBER_QRY) {
			return new ErrorResponseEntity("Number out of range 1.." + MAX_CUSTOM_ACCOUNT_NUMBER_QRY);
		}
		return new ResponseEntity<>(accountRepository.findAllByOrderByLastTransactionDesc(new PageRequest(0, number)),
				HttpStatus.OK);
	}

	@RequestMapping("/totalbalance")
	@ResponseBody
	TotalBalanceHolder getSystemBalance() {
		return new TotalBalanceHolder(accountRepository.findSystemBalance());
	}
}
