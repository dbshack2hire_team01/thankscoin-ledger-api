package com.dbshac2hire.thankcoin.ledger.api.controller.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


public class SuccessResponseEntity extends ResponseEntity<Object> {

	public SuccessResponseEntity(String message) {
		super(new SuccessMessage(message), HttpStatus.OK);
	}

	private static class SuccessMessage {
		private final String success;

		public SuccessMessage(String success) {
			this.success = success;
		}

		@SuppressWarnings("unused")
		public String getError() {
			return success;
		}
	}
}
