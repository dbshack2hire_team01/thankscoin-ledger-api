package com.dbshac2hire.thankcoin.ledger.api.controller;

import java.util.Date;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbshac2hire.thankcoin.ledger.api.controller.util.ErrorResponseEntity;
import com.dbshac2hire.thankcoin.ledger.api.domain.Account;
import com.dbshac2hire.thankcoin.ledger.api.domain.Transaction;
import com.dbshac2hire.thankcoin.ledger.api.domain.Transaction.Type;
import com.dbshac2hire.thankcoin.ledger.api.repository.AccountRepository;
import com.dbshac2hire.thankcoin.ledger.api.repository.TransactionRepository;

@Controller
@EnableAutoConfiguration
public class TransactionController {
	private static final Logger log = Logger.getLogger(TransactionController.class);

	@Autowired
	AccountRepository accountRepository;
	@Autowired
	TransactionRepository transactionRepository;

	@RequestMapping("/transactions")
	@ResponseBody
	Iterable<Transaction> getTransactions(@RequestParam(required = false) String accountFrom,
			@RequestParam(required = false) String accountTo, @RequestParam(required = false) Long afterId,
			@RequestParam(required = false) Long amount,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") Date txtimeFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") Date txtimeTo) {
		return transactionRepository.search(accountFrom, accountTo, afterId, amount, txtimeFrom, txtimeTo);
	}

	@RequestMapping("/transactionshistory")
	@ResponseBody
	Iterable<Transaction> getTransactionsHistory(@RequestParam String accountNumber) {
		return transactionRepository.findByAccountFromOrAccountToOrderByTimestampDesc(accountNumber, accountNumber);
	}

	@RequestMapping("/extransactions")
	@ResponseBody
	Iterable<Transaction> getExtendedTransactions(@RequestParam(required = false) String accountFrom,
			@RequestParam(required = false) String accountTo, @RequestParam(required = false) Long afterId,
			@RequestParam(required = false) Long amount,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") Date txtimeFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") Date txtimeTo) {
		return transactionRepository.extendedSearch(accountFrom, accountTo, afterId, amount, txtimeFrom, txtimeTo);
	}

	@RequestMapping("/transaction/{id}")
	@ResponseBody
	ResponseEntity<?> getTransaction(@PathVariable Long id) {
		Optional<Transaction> transaction = transactionRepository.findOne(id);
		if (transaction.isPresent()) {
			return new ResponseEntity<Transaction>(transaction.get(), HttpStatus.OK);
		}
		return new ErrorResponseEntity("Transaction is not found", HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/transaction", method = { RequestMethod.POST })
	@ResponseBody
	@Transactional
	ResponseEntity<?> createTransaction(@RequestBody CreateTransactionRequest createTransactionRequest) {
		try {
			String accountFrom = createTransactionRequest.getAccountFrom();
			if (accountFrom == null || accountFrom.trim().isEmpty()) {
				return new ErrorResponseEntity("Missing accountFrom field");
			}
			String accountTo = createTransactionRequest.getAccountTo();
			if (accountTo == null || accountTo.trim().isEmpty()) {
				return new ErrorResponseEntity("Missing accountTo field");
			}
			Long amountObject = createTransactionRequest.getAmount();
			if (amountObject == null) {
				return new ErrorResponseEntity("Missing amount field");
			}
			long amount = amountObject;
			if (amount <= 0) {
				log.warn("Failed to create transaction because supplied amount is zero or negative: " + amount
						+ " accountFrom: " + accountFrom + ", accountTo: " + accountTo);
				return new ErrorResponseEntity("Zero or negative amount if prohibited");
			}

			Optional<Account> accountObjectOptionalFrom = accountRepository.findByNumber(accountFrom);
			if (!accountObjectOptionalFrom.isPresent()) {
				log.warn("Account to deduct is not found: accountFrom: " + accountFrom + ", accountTo: " + accountTo);
				return new ErrorResponseEntity("Account to deduct is not found");
			}
			Account accountObjectFrom = accountObjectOptionalFrom.get();
			if (accountObjectFrom.getBalance() < amount) {
				log.warn("Account to deduct has low balance: accountFrom: " + accountFrom + ", accountTo: " + accountTo
						+ " balance: " + accountObjectFrom.getBalance() + ", amount: " + amount);
				return new ErrorResponseEntity("Not enough money");
			}

			Optional<Account> accountObjectOptionalTo = accountRepository.findByNumber(accountTo);
			if (!accountObjectOptionalTo.isPresent()) {
				log.warn("Account to add is not found: accountFrom: " + accountFrom + ", accountTo: " + accountTo);
				return new ErrorResponseEntity("Account to add is not found");
			}
			Account accountObjectTo = accountObjectOptionalTo.get();
			if (accountObjectFrom.getId().equals(accountObjectTo.getId())) {
				log.warn("Same account for both sides: accountFrom: " + accountFrom + ", accountTo: " + accountTo);
				return new ErrorResponseEntity("Same account for both sides");
			}
			if (accountObjectTo.getBalance() > Long.MAX_VALUE - amount) {
				log.warn("Account to add has high balance: accountFrom: " + accountFrom + ", accountTo: " + accountTo
						+ " balance: " + accountObjectTo.getBalance() + ", amount: " + amount);
				return new ErrorResponseEntity("To much money on destination account");
			}

			Date transactionTime = new Date();
			accountObjectFrom.setBalance(accountObjectFrom.getBalance() - amount);
			accountObjectFrom.setLastTransaction(transactionTime);
			accountRepository.save(accountObjectFrom);
			accountObjectTo.setBalance(accountObjectTo.getBalance() + amount);
			accountObjectTo.setLastTransaction(transactionTime);
			accountRepository.save(accountObjectTo);
			Transaction transaction = transactionRepository
					.save(new Transaction(Type.TRANSFER, accountFrom, accountTo, amount, transactionTime,
							createTransactionRequest.getDescription(), createTransactionRequest.getSignature()));
			return new ResponseEntity<>(transaction, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error while creating transaction", e);
			return new ErrorResponseEntity("Unexpected error while creating transaction");
		}
	}
}
