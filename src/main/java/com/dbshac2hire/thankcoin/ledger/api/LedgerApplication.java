package com.dbshac2hire.thankcoin.ledger.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LedgerApplication {

	/**
	 * The main application method, bootstraps the Spring container.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(LedgerApplication.class, args);
	}

}
