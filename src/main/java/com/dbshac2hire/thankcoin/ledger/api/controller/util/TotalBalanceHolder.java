package com.dbshac2hire.thankcoin.ledger.api.controller.util;

public class TotalBalanceHolder {
	private String totalBalance;

	public TotalBalanceHolder(String totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getTotalBalance() {
		return totalBalance;
	}
}
