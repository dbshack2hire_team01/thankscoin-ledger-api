package com.dbshac2hire.thankcoin.ledger.api.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Account {
	private @Id @GeneratedValue Long id;
	private final @Column(unique=true) String number;
	private final boolean open;
	private long balance;
	private final Date created;
	private Date lastTransaction;
	
	public Account(String number, boolean open, long balance, Date created, Date lastTransaction) {
		Assert.hasText(number, "Name must not be null or empty!");
		
		this.number = number;
		this.open = open;
		this.balance = balance;
		this.created = created;
		this.lastTransaction = lastTransaction;
	}
	
	@SuppressWarnings("unused")
	private Account() {
		id = null;
		number = null;
		open = false;
		balance = 0;
		created = null;
		lastTransaction = null;
	}
	
	@JsonIgnore
	public Long getId() {
		return id;
	}

	public String getNumber() {
		return number;
	}

	public boolean isOpen() {
		return open;
	}

	public long getBalance() {
		return balance;
	}
	
	public void setBalance(long balance) {
		this.balance = balance;
	}

	public Date getCreated() {
		return created;
	}

	public Date getLastTransaction() {
		return lastTransaction;
	}
	
	public void setLastTransaction(Date lastTransaction) {
		this.lastTransaction = lastTransaction;
	}
	
	
}
